import math
 

def DoubleSqrt(a,b,c):
    discr = b ** 2 - 4 * a * c
 
    if discr > 0:
        x1 = (-b + math.sqrt(discr)) / (2 * a)
        x2 = (-b - math.sqrt(discr)) / (2 * a)
    return max(x1, x2)